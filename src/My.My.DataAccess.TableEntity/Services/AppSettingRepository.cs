using Microsoft.Extensions.Options;
using My.My.DataAccess.Core.Interfaces;
using My.My.DataAccess.Core.Models;
using My.My.DataAccess.TableEntity.Repositories;
using My.My.Util.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My.My.DataAccess.TableEntity.Services
{
    public class AppSettingRepository : Repository<AppSetting>, IAppSettingRepository
    {
        public AppSettingRepository(IOptions<StorageAccountOptions> options) : base(options, "AppSetting")
        {

        }
        
    }
}
