using My.My.DataAccess.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace My.My.DataAccess.Core.Interfaces
{
    public interface IAppSettingRepository : IRepository<AppSetting>
    {
        
    }
}
