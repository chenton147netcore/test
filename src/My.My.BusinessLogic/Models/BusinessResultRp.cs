using Newtonsoft.Json;
using My.My.BusinessLogic.Core;
using My.My.BusinessLogic.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace My.My.BusinessLogic.Models
{
    public class BusinessResultRp
    {
        private List<BusinessManagerMessage> _messages;
        private Dictionary<string, string> _results;

        public BusinessResultRp()
        {
            _messages = new List<BusinessManagerMessage>();
            _results = new Dictionary<string, string>();
        }

        public void AddConflict(string message)
        {
            BusinessManagerMessage domainMessage = new BusinessManagerMessage("", message, BusinessManagerMessageType.Conflict);
            _messages.Add(domainMessage);
        }

        public void AddNotFound(string message)
        {
            BusinessManagerMessage domainMessage = new BusinessManagerMessage("", message, BusinessManagerMessageType.NotFound);
            if (!_messages.Any(c => c.BusinessManagerMessageType == BusinessManagerMessageType.NotFound))
                _messages.Add(domainMessage);
        }

        public void AddResult(string key, object value)
        {
            _results.Add(key, JsonConvert.SerializeObject(value));
        }

        public T GetResult<T>(string key)
        {
            if (!_results.Keys.Any(x => x.Equals(key, StringComparison.InvariantCultureIgnoreCase)))
                throw new KeyNotFoundException($"The key ${key} was not found");

            return JsonConvert.DeserializeObject<T>(_results[key]);
        }

        public string GetConflicts()
        {
            StringBuilder stringBuilder = new StringBuilder();
            var conflicts = _messages.Where(x => x.BusinessManagerMessageType == BusinessManagerMessageType.Conflict);
            foreach (var item in conflicts)
            {
                stringBuilder.AppendLine(item.Value);
            }
            return stringBuilder.ToString();
        }

        public bool HasConflicts()
        {
            return _messages.Any(x => x.BusinessManagerMessageType == BusinessManagerMessageType.Conflict);
        }

        public void Dispose()
        {
            _messages = new List<BusinessManagerMessage>();
        }

        public string GetNotFounds()
        {
            StringBuilder stringBuilder = new StringBuilder();
            var conflicts = _messages.Where(x => x.BusinessManagerMessageType == BusinessManagerMessageType.NotFound);
            foreach (var item in conflicts)
            {
                stringBuilder.AppendLine(item.Value);
            }
            return stringBuilder.ToString();
        }

        public bool HasNotFounds()
        {
            return _messages.Any(x => x.BusinessManagerMessageType == BusinessManagerMessageType.NotFound);
        }
    }
}
