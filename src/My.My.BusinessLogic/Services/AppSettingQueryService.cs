using My.My.BusinessLogic.Constants;
using My.My.BusinessLogic.Models;
using My.My.BusinessLogic.Services.Interfaces;
using My.My.DataAccess.Core.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace My.My.BusinessLogic.Services
{
    public class AppSettingQueryService : IAppSettingQueryService
    {
        private readonly IAppSettingRepository _appSettingDataService;

        public AppSettingQueryService(IAppSettingRepository appSettingDataService)
        {
            this._appSettingDataService = appSettingDataService;
        }

        public async Task<AppSettingGetRp> GetAppSettingById(string id)
        {
            var entity = await this._appSettingDataService.GetById(DefaultConstantNames.DefaultPartitionKey, id);
            return new AppSettingGetRp { Id = entity.RowKey, Value = entity.Value };
        }

        public async Task<AppSettingListRp> GetAppSettings()
        {
            var entities = await this._appSettingDataService.GetAll();

            return new AppSettingListRp() {
                Items = entities.Select(entity => new AppSettingListItemRp { Id = entity.RowKey, Value = entity.Value }).ToList()
            };
        }
    }
}
