using My.My.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace My.My.BusinessLogic.Services.Interfaces
{
    public interface IAppSettingService
    {
        Task<BusinessResultRp> CreateAppSetting(AppSettingPostRp resource);
        Task<BusinessResultRp> UpdateAppSetting(string id, AppSettingPutRp resource);
        Task<BusinessResultRp> DeleteAppSetting(string id);
    }
}
