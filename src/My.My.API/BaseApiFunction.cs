using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My.My.API
{
    public abstract class BaseApiFunction
    {

        public BadRequestObjectResult BadRequest(string message)
        {
            return new BadRequestObjectResult(message);
        }

        public NotFoundObjectResult NotFound(string message)
        {
            return new NotFoundObjectResult(message);
        }

        public ConflictObjectResult Conflict(string message)
        {
            return new ConflictObjectResult(message);
        }

        public OkObjectResult Ok(object message)
        {
            return new OkObjectResult(message);
        }

        public OkObjectResult Ok()
        {
            return this.Ok(new { });
        }

        public NoContentResult NoContent()
        {
            return new NoContentResult();
        }
    }
}
