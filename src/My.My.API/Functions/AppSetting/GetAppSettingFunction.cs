using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using My.My.BusinessLogic.Services.Interfaces;

namespace My.My.API.Functions.AppSetting
{
    public class GetAppSettingFunction : BaseApiFunction
    {
        private readonly IAppSettingQueryService _appSettingQueryService;

        public GetAppSettingFunction(IAppSettingQueryService appSettingQueryService)
        {
            this._appSettingQueryService = appSettingQueryService;
        }

        [FunctionName("GetAppSettingFunction")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", 
            Route = "appsettings")] HttpRequest req,
            ILogger log)
        {
            var model = await this._appSettingQueryService.GetAppSettings();
            return this.Ok(model);
        }
    }
}
