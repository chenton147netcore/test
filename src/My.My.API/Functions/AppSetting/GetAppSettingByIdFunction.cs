using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using My.My.BusinessLogic.Services.Interfaces;

namespace My.My.API.Functions.AppSetting
{
    public class GetAppSettingByIdFunction : BaseApiFunction
    {
        private readonly IAppSettingQueryService _appSettingQueryService;

        public GetAppSettingByIdFunction(IAppSettingQueryService appSettingQueryService)
        {
            this._appSettingQueryService = appSettingQueryService;
        }

        [FunctionName("GetAppSettingByIdFunction")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "appsettings/{id}")] HttpRequest req,
            ILogger log, string id)
        {
            var model = await this._appSettingQueryService.GetAppSettingById(id);
            return this.Ok(model);
        }
    }
}
