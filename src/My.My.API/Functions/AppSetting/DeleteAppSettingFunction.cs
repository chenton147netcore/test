using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using My.My.BusinessLogic.Services.Interfaces;

namespace My.My.API.Functions.AppSetting
{
    public class DeleteAppSettingFunction : BaseApiFunction
    {
        private readonly IAppSettingService _appSettingService;

        public DeleteAppSettingFunction(IAppSettingService appSettingService)
        {
            this._appSettingService = appSettingService;
        }

        [FunctionName("DeleteAppSettingFunction")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "appsettings/{id}")] HttpRequest req,
            ILogger log,
            string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return this.BadRequest($"Missing required parameter id");
            }

            var response = await this._appSettingService.DeleteAppSetting(id);

            if (response.HasNotFounds())
            {
                return this.NotFound(response.GetNotFounds());
            }

            if (response.HasConflicts())
            {
                return this.Conflict(response.GetConflicts());
            }

            return this.NoContent();
        }
    }
}
