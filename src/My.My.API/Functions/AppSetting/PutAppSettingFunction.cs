using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using My.My.BusinessLogic.Services.Interfaces;
using My.My.BusinessLogic.Models;

namespace My.My.API.Functions.AppSetting
{
    public class PutAppSettingFunction : BaseApiFunction
    {
        private readonly IAppSettingService _appSettingService;

        public PutAppSettingFunction(IAppSettingService appSettingService)
        {
            this._appSettingService = appSettingService;
        }

        [FunctionName("PutAppSettingFunction")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "put", Route = "appsettings/{id}")] HttpRequest req,
            ILogger log, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return this.BadRequest($"Missing required parameter id");
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var resource = JsonConvert.DeserializeObject<AppSettingPutRp>(requestBody);
            var response = await this._appSettingService.UpdateAppSetting(id, resource);

            if (response.HasNotFounds())
            {
                return this.NotFound(response.GetNotFounds());
            }

            if (response.HasConflicts())
            {
                return this.Conflict(response.GetConflicts());
            }

            return this.Ok();
        }
    }
}
