using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using My.My.BusinessLogic.Services.Interfaces;
using My.My.BusinessLogic.Models;

namespace My.My.API.Functions.AppSetting
{
    public class PostAppSettingFunction : BaseApiFunction
    {
        private readonly IAppSettingService _appSettingService;

        public PostAppSettingFunction(IAppSettingService appSettingService)
        {
            this._appSettingService = appSettingService;
        }

        [FunctionName("PostAppSettingFunction")]
        public async Task<IActionResult> Run(
           [HttpTrigger(AuthorizationLevel.Function, "post",
            Route = "appsettings")] HttpRequest req,
           ILogger log)
        {

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var resource = JsonConvert.DeserializeObject<AppSettingPostRp>(requestBody);
            var response = await this._appSettingService.CreateAppSetting(resource);

            if (response.HasConflicts())
            {
                return this.Conflict(response.GetConflicts());
            }

            var id = response.GetResult<string>("Key");

            return this.Ok(new { Id = id });
        }
    }
}
