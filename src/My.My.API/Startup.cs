using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using My.My.API;
using My.My.CrossCutting.IoC;
using My.My.Util.Options;
using UtilOptions = My.My.Util.Options;

[assembly: WebJobsStartup(typeof(Startup))]
namespace My.My.API
{
    internal class Startup : IWebJobsStartup
    {
       
        public void Configure(IWebJobsBuilder builder)
        {
            string functionDirectory = Environment.GetEnvironmentVariable("FUNCTION_DIRECTORY");
            if (string.IsNullOrEmpty(functionDirectory))
                functionDirectory = Directory.GetCurrentDirectory();

            IConfigurationRoot Configuration = new ConfigurationBuilder()
                .SetBasePath(functionDirectory)
                .AddJsonFile("local.settings.json", optional: true)
                .AddJsonFile($"local.settings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            builder.Services.Configure<StorageAccountOptions>(options=> {
                options.StorageConnectionString = Configuration["ConnectionStrings:StorageConnectionString"];
            });
            
            builder.Services.AddApplicationServices(Configuration);
            builder.Services.AddRepositories(Configuration);
        }
    }
}
